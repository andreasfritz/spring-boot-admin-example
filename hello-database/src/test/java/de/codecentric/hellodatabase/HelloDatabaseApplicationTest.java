package de.codecentric.hellodatabase;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = HelloDatabaseApplication.class)
class HelloDatabaseApplicationTest {

  @Test
  void contextLoads() {
    assertTrue(true);
  }
}
