package de.codecentric.hellodatabase;

import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Slf4j
public class HelloDatabase {

  private final JdbcTemplate jdbcTemplate;

  @GetMapping
  public String hello() {
    jdbcTemplate.execute(
        "CREATE TABLE IF NOT EXISTS greetings (id UUID PRIMARY KEY, message VARCHAR(255))");
    jdbcTemplate.update("INSERT INTO greetings (id, message) VALUES (?, ?)", UUID.randomUUID(),
        "Hello Database!");
    jdbcTemplate.queryForList("SELECT * FROM greetings")
        .forEach(select -> log.info(select.toString()));
    var count = jdbcTemplate.queryForObject("SELECT COUNT(*) FROM greetings", Integer.class);
    return "Hello Database! %s greetings found.".formatted(count);
  }

}
