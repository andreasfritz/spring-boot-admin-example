package de.codecentric.hellocloud;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloCloud {

  @GetMapping
  public String hello() {
    return "Hello Cloud!";
  }

}
