package de.codecentric.hellocloud.jmx;

import java.util.HashMap;
import java.util.Map;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Component;

@Component
@ManagedResource
public class StringMapManagedBean {

  private final Map<String, String> map = new HashMap<>();

  @ManagedAttribute
  public int getSize() {
    return map.size();
  }

  @ManagedOperation
  public String get(String key) {
    return map.get(key);
  }

  @ManagedOperation
  public String put(String key, String value) {
    return map.put(key, value);
  }

  @ManagedOperation
  public void remove(String key) {
    map.remove(key);
  }
}
