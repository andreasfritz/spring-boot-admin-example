package de.codecentric.hellocloud;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = HelloCloudApplication.class)
class HelloCloudApplicationTest {

  @Test
  void contextLoads() {
    assertTrue(true);
  }
}
