# README

## Start
    mvn spring-boot:run

## URL
- http://localhost:9090/
- http://localhost:9091/actuator
- http://localhost:9091/actuator/health/owngroup

## Für zwei Instanzen
SERVER_PORT=9092;MANAGEMENT_SERVER_PORT=9093
