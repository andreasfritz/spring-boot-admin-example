# README

- http://localhost:9000/
- http://localhost:9000/insecure
- http://localhost:9000/actuator

## Build Docker Image

    mvn spring-boot:build-image -Dspring-boot.build-image.imageName=hello-secure:latest
    docker network create sba-network
    docker-compose up

## Build Native

    sdk install java 22.3.r17-grl
    sdk use java 22.3.r17-grl
    mvn clean package -Pnative
