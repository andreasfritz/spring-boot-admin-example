package de.codecentric.hellosecure;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloSecure {

  @GetMapping("/")
  public String hello() {
    return "Hello Secure!";
  }

}
