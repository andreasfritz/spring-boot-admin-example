package de.codecentric.hellosecure;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloInsecure {

  @GetMapping("/insecure")
  public String hello() {
    return "Hello Insecure!";
  }

}
