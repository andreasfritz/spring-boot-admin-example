package de.codecentric.springbootadmin.security;

import static org.springframework.http.HttpMethod.DELETE;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.security.config.Customizer.withDefaults;

import jakarta.servlet.DispatcherType;
import org.springframework.boot.actuate.audit.AuditEventRepository;
import org.springframework.boot.actuate.audit.InMemoryAuditEventRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.csrf.CsrfTokenRequestAttributeHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@Profile("!nosecurity")
public class WebSecurityConfig {

  @Bean
  public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {

    http.authorizeHttpRequests(
        authorizeHttpRequests ->
            authorizeHttpRequests
                .requestMatchers(new AntPathRequestMatcher("/assets/**"))
                .permitAll()
                .requestMatchers(new AntPathRequestMatcher("/actuator/**"))
                .permitAll()
                .requestMatchers(new AntPathRequestMatcher( "/instances",HttpMethod.POST.toString()))
                .permitAll()
                .requestMatchers(new AntPathRequestMatcher( "/instances/*",HttpMethod.DELETE.toString()))
                .permitAll()
                .dispatcherTypeMatchers(DispatcherType.ASYNC).permitAll()
                .anyRequest().fullyAuthenticated())
        .oauth2Login(withDefaults());

    http.addFilterAfter(new CustomCsrfFilter(), BasicAuthenticationFilter.class)
        .csrf(csrf -> csrf.csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
            .csrfTokenRequestHandler(new CsrfTokenRequestAttributeHandler()).ignoringRequestMatchers(
                new AntPathRequestMatcher("/instances", POST.toString()),
                new AntPathRequestMatcher("/instances/*", DELETE.toString()),
                new AntPathRequestMatcher("/actuator/**")
        ));

    return http.build();
  }

  @Bean
  public AuditEventRepository auditEventRepository() {
    return new InMemoryAuditEventRepository();
  }
}
