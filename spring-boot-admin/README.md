# README

## Start
    mvn spring-boot:run

### No Security
    java -Dspring.profiles.active=nosecurity -jar target/spring-boot-admin.jar

### IntelliJ - Environment Variables
    OAUTH2_DIRECTORY_ID
    OAUTH2_CLIENT_ID
    OAUTH2_CLIENT_SECRET

## URL
- http://localhost:8080/wallboard
- http://localhost:8081/actuator

## Spring Releases

- https://github.com/spring-projects/spring-framework/releases/
- https://github.com/spring-projects/spring-boot/releases
- https://github.com/spring-cloud/spring-cloud-release/releases

## Spring Milestones

- https://github.com/spring-projects/spring-boot/milestones
- https://github.com/spring-cloud/spring-cloud-release/milestones
- https://calendar.spring.io/

## Build Docker Image

    mvn spring-boot:build-image -Dspring-boot.build-image.imageName=spring-boot-admin:latest
    docker network create sba-network
    docker-compose up

## Build Native

    sdk install java 22.3.r17-grl
    sdk use java 22.3.r17-grl
    mvn clean package -Pnative
    ./spring-boot-admin
    org.thymeleaf.exceptions.TemplateInputException: Error resolving template [index], template might not exist or might not be accessible by any of the configured Template Resolvers
