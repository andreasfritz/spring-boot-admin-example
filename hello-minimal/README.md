# README

## Start
    mvn spring-boot:run

## URL
- http://localhost:9099/
- http://localhost:9099/actuator/health
- http://localhost:9099/actuator/info
