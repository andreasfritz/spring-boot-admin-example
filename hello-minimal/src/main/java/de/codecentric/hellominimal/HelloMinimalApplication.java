package de.codecentric.hellominimal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelloMinimalApplication {

  public static void main(String... args) {
    SpringApplication.run(HelloMinimalApplication.class);
  }
}
