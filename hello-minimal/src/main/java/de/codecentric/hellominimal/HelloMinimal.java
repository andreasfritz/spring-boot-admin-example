package de.codecentric.hellominimal;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloMinimal {

  @GetMapping("/")
  public String hello() {
    return "Hello Minimal!";
  }
}
