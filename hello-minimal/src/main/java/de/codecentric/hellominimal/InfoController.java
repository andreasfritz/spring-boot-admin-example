package de.codecentric.hellominimal;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InfoController {

  @GetMapping("/actuator/info")
  public String info() {
    return """
      {"group":"de.codecentric","artifact":"hello-minimal","description":"Hello Minimal","version":"Eine Version","tags":{"Hallo":"Welt"}}
    """;
  }

  //  @GetMapping("/actuator/info")
  //  public ResponseEntity info() {
  //    return ResponseEntity.status(HttpStatus.BAD_GATEWAY).build();
  //  }
}
