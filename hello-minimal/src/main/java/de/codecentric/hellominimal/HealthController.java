package de.codecentric.hellominimal;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthController {

  @GetMapping("/actuator/health")
  public String health() {
    return """
      {"status":"UP"}
    """;
  }

  //  @GetMapping("/actuator/health")
  //  public ResponseEntity health() {
  //    return ResponseEntity.status(HttpStatus.BAD_GATEWAY).build();
  //  }
}
