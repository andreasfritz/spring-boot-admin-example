# README

## Start
    mvn spring-boot:run

## URL
- http://localhost:9080/api
- http://localhost:9080/api/up
- http://localhost:9080/api/down
- http://localhost:9081/management/act
- http://localhost:9081/management/act/health/liveness
- http://localhost:9081/management/act/health/readiness
- http://localhost:9081/management/act/health/owngroup

## Für zwei Instanzen
SERVER_PORT=9082;MANAGEMENT_SERVER_PORT=9083
