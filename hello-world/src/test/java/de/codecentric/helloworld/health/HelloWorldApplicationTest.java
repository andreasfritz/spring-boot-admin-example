package de.codecentric.helloworld.health;

import static org.junit.jupiter.api.Assertions.assertTrue;

import de.codecentric.helloworld.HelloWorldApplication;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = HelloWorldApplication.class)
class HelloWorldApplicationTest {

  @Test
  void contextLoads() {
    assertTrue(true);
  }
}
