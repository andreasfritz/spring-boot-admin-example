package de.codecentric.helloworld.health;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.Status;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {HelloHealthIndicator.class})
class HelloHealthIndicatorTest {

  @Autowired private HelloHealthIndicator helloHealthIndicator;

  @Test
  void up() {
    // Given
    helloHealthIndicator.setHealthy(true);

    // When
    Health health = helloHealthIndicator.health();

    // Then
    assertEquals(Status.UP, health.getStatus());
    assertEquals("world", health.getDetails().get("Hello"));
  }

  @Test
  void down() {
    // Given
    helloHealthIndicator.setHealthy(false);

    // When
    Health health = helloHealthIndicator.health();

    // Then
    assertEquals(Status.DOWN, health.getStatus());
    assertEquals("world", health.getDetails().get("Bye bye"));
  }
}
