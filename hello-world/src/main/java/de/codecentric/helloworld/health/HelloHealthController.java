package de.codecentric.helloworld.health;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class HelloHealthController {

  private final HelloHealthIndicator helloHealthIndicator;

  @GetMapping("/up")
  public String up() {

    helloHealthIndicator.setHealthy(true);
    return "up";
  }

  @GetMapping("/down")
  public String down() {

    helloHealthIndicator.setHealthy(false);
    return "down";
  }
}
