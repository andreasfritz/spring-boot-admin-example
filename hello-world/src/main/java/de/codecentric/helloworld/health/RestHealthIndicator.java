package de.codecentric.helloworld.health;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component("downstream")
public class RestHealthIndicator implements HealthIndicator {

  RestTemplate restTemplate = new RestTemplate();

  @Value("${my.downstream.service.url:https://www.google.de/}")
  private String downstreamUrl;

  @Override
  public Health health() {
    try {
      ResponseEntity<String> responseEntity =
          restTemplate.getForEntity(downstreamUrl, String.class);
      HttpStatus httpStatus = responseEntity.getStatusCode();
      if (httpStatus.is2xxSuccessful()) {
        return Health.up()
            .withDetail("httpStatus", httpStatus)
            // .withDetail("response", responseEntity.getBody())
            .build();
      } else {
        return Health.down().withDetail("httpStatus", httpStatus).build();
      }
    } catch (Exception e) {
      return Health.down().withException(e).build();
    }
  }
}
