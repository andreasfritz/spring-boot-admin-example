package de.codecentric.helloworld.health;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component("hello")
@Slf4j
public class HelloHealthIndicator implements HealthIndicator {

  private boolean healthy = true;

  @Override
  public Health health() {
    if (healthy) {
      return Health.up().withDetail("Hello", "world").build();
    } else {
      return Health.down().withDetail("Bye bye", "world").build();
    }

    // return Health.unknown().withDetail("max", "mustermann").withDetail("hallo", "welt").build();
    //    return Health.down()
    //        .withException(new OutOfMemoryError("Hallo"))
    //        .withDetail("max", "mustermann")
    //        .withDetail("hallo", "welt")
    //        .build();
    // return Health.status("WARNING").withDetail("max", "mustermann").withDetail("hallo",
    // "welt").build();
    //    return Health.outOfService()
    //        .withDetail("max", "mustermann")
    //        .withDetail("hallo", "welt")
    //        .build();

  }

  public void setHealthy(boolean healthy) {
    this.healthy = healthy;
  }
}
