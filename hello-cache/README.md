# README

## Start

    mvn spring-boot:run

## URL

- http://localhost:9090/
- http://localhost:9091/actuator
- http://localhost:9091/actuator/metrics
- http://localhost:9091/actuator/metrics/cache.gets
- http://localhost:9091/actuator/health/owngroup
- http://localhost:9091/actuator/caches
