package de.codecentric.hellocache;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = HelloCacheApplication.class)
class HelloCacheApplicationTest {

  @Test
  void contextLoads() {
    assertTrue(true);
  }
}
